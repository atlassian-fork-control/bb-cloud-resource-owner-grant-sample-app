
Bitbucket Cloud Resource Owner Grant Sample App
---

## Introduction
This app aims to demonstrate the Resource Owner Grant Type Authorization flow, what is needed to generate an access_token and how to use that to invoke Bitbucket REST APIs, and lastly a reference app where the developer can copy paste the code and try it in their own projects

## What is Resource Owner Grant?
The resource owner password credentials (i.e. username and password) can be used directly as an authorization grant to obtain an access token. The credentials should only be used when there is a high degree of trust between the resource owner and the client (e.g. the client is part of the device operating system or a highly privileged application), and when other authorization grant types are not available (such as an authorization code)

Useful when you have the end user’s password but you want to use a more secure end user access token instead. **This method will not work when the user has two-step verification enabled**

### Difference with other grants
- Don't need user initiation
- Can be used directly to obtain an access_token
- Uses the user's credentials to authenticate
- Resource owner credentials are used at a single request and are exchanged for an access_token. Eliminating the need for the client to store the end-users credentials by exchanging a long lived access_token/refresh_token

### Authorization Flow Chart
![Alt text](public/images/authorization_flow.png?raw=true "Bitbucket Authorization Grant Flow")

### Things to consider
*What is OAuth?*

OAuth is a mechanism for authorizing an entity and granting them access over which the user is capable of doing in an application

*What it is not?*

OAuth is not a way to authenticate but rather to authorize an entity for access

*Definition of terms*

| Term          									| Description   |
|---------------------------------------------------|-------------|
|Basic Auth    										|Is an authentication mechanism. Is straightforward but unsecure. It is the combination of your username and password that you would use to access an API endpoint.|
|client_id     										|Key generated from Bitbucket Settings → Add Consumer   |
|secret        										|Secret generated from Bitbucket Settings → Add Consumer|
|https://bitbucket.org/site/oauth2/authorize        |URL that you use to authorize an entity|
|https://bitbucket.org/site/oauth2/access_token		|URL that generates an access token for an entity|
|access_token										|Defines the permission of the selected user, ergo represents the authorization of a specific application to access specific parts of a user's data. Tokens that represents the authorization of an entity to access specific parts of a user's data in an application. It is the token generated or returned by https://bitbucket.org/site/oauth2/access_token|

*Participants*

| Role          									| Description   |
|---------------------------------------------------|-------------|
|Resource Owner/End-user    						|An entity capable of granting access to a protected resource. When the resource owner is a person, it is referred to as an end- user.|
|Client     										|An application making protected resource requests on behalf of the resource owner and with its authorization.|
|Resource Server        							|The server hosting the protected resources, capable of accepting and responding to protected resource requests using access tokens.|
|Authorization Server						       	|The server issuing access tokens to the client after successfully authenticating the resource owner and obtaining authorization.|

## Prerequisite
1. NodeJs

## Running the app
1. You are free to use the consumer key and secret configured in this project or generate your own. If you are [creating your own oauth consumer](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html#OAuthonBitbucketCloud-Createaconsumer), please make sure to set the callback URL to `http://localhost:3000/oauth-callback` as `/oauth-callback` processes the response from Bitbucket's Authorization server, just replace the consumerKey and consumerSecret in config.json and you're good to go.
2. In the project's root directory, run `npm install`. This should download all the dependencies for you needed by the app
3. Once download is complete, run `npm start`
4. Open your browser and launch: http://localhost:3000/ , this should display an intuitive page on how to go through the authorization flow

## Navigating the app

| Endpoint 			| Description |
|-------------------|-------------|
| /					|Redirects the user to the landing page of the sample app |
| /healthcheck		|Test whether your app is alive							|
| /oauth-callback	|Initiates getting the access_token|

## Support

If you have any questions about this app or simply want to give us feedback please do so in the [Bitbucket Cloud category](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud) on the Developer Community.

## Related Documentation
- [Bitbucket OAuth 2.0](https://developer.atlassian.com/cloud/bitbucket/oauth-2/)
- [IETF - Resource Owner Grant](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.3)
- [OAuth Participants](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.1)
